git pull
python -m venv tortoise-venv
source ./tortoise-venv/bin/activate
python -m pip install --upgrade pip
python -m pip install -r ./requirements.txt
deactivate